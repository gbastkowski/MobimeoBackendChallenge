import Dependencies._

ThisBuild / organization          :=  "challenge.mobimeo.com"
ThisBuild / resolvers             ++= Seq("confluent" at "https://packages.confluent.io/maven/")    ++
                                      Seq("jitpack"   at "https://jitpack.io")                      ++
                                      Seq(Resolver.sonatypeRepo("snapshots"))
ThisBuild / scalacOptions         ++= Seq(
                                        "-feature",
                                        "-deprecation",
                                        "-unchecked",
                                        "-language:postfixOps")


lazy val app = project.in(file("."))
  .configs(IntegrationTest)
  .settings(Defaults.itSettings)
  .settings(
    name                           := "verspaetung-api",
    scalaVersion                   := "3.1.0",
    Test / fork                    := true,
    Test / logBuffered             := false,
    IntegrationTest / fork         := true,
    IntegrationTest / logBuffered  := false,
    libraryDependencies           ++= cats                                                          ++
                                      circeCodecs                                                   ++
                                      circeParser                                                   ++
                                      fs2Data                                                       ++
                                      http4sServer                                                  ++
                                      logging                                                       ++
                                      openapi                                                       ++
                                      scalatest     .map(_ % "test,it")                             ++
                                      http4sClient)
