# Mobimeo Backend Engineering Challenge

## Solution

### Run the tests

```
sbt test
sbt it:test
```

### Run the app

```
sbt run
```

## Data

Sample requests are in `requests.http`.
