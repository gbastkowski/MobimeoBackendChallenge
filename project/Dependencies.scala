import sbt._
import Keys._

object Dependencies {
  val CatsVersion                   = "2.7.0"
  val CirceVersion                  = "0.14.1"
  val Fs2DataVersion                = "1.3.1"
  val Http4sVersion                 = "0.23.7"
  val Log4jVersion                  = "2.17.0"
  val ScalatestVersion              = "3.2.10"
  val SwaggerUiVersion              = "3.24.3"
  val TapirVersion                  = "0.19.3"
  val TestcontainersVersion         = "0.39.11"

  val cats = Seq(
    "org.typelevel"                %% "cats-core"                       % CatsVersion)

  val circeCodecs = Seq(
    "io.circe"                     %% "circe-generic"                   % CirceVersion,
    // "io.circe"                     %% "circe-literal"                   % CirceVersion,
  )

  val circeParser = Seq(
    "io.circe"                     %% "circe-parser"                    % CirceVersion)

  val fs2Data = Seq(
    "org.gnieh"                    %% "fs2-data-csv"                    % Fs2DataVersion,
    "org.gnieh"                    %% "fs2-data-csv-generic"            % Fs2DataVersion)

  val http4sClient = Seq(
    "org.http4s"                   %% "http4s-blaze-client"             % Http4sVersion,
    "org.http4s"                   %% "http4s-circe"                    % Http4sVersion)

  val http4sServer = Seq(
    "org.http4s"                   %% "http4s-dsl"                      % Http4sVersion,
    "org.http4s"                   %% "http4s-blaze-server"             % Http4sVersion)

  val logging = Seq(
    "org.apache.logging.log4j"      % "log4j-api"                       % Log4jVersion,
    "org.apache.logging.log4j"      % "log4j-core"                      % Log4jVersion,
    "org.apache.logging.log4j"       % "log4j-slf4j-impl"               % Log4jVersion        % Runtime)

  val openapi = Seq(
    "com.softwaremill.sttp.tapir"  %% "tapir-core"                      % TapirVersion,
    "com.softwaremill.sttp.tapir"  %% "tapir-http4s-server"             % TapirVersion,
    "com.softwaremill.sttp.tapir"  %% "tapir-json-circe"                % TapirVersion,
    "com.softwaremill.sttp.tapir"  %% "tapir-openapi-circe-yaml"        % TapirVersion,
    "com.softwaremill.sttp.tapir"  %% "tapir-openapi-docs"              % TapirVersion,
    // "com.softwaremill.sttp.tapir"  %% "tapir-redoc-http4s"              % TapirVersion,
  )

  val scalatest = Seq(
    "org.scalatest"                %% "scalatest"                       % ScalatestVersion)

  val testcontainers = Seq(
    "com.dimafeng"                 %% "testcontainers-scala-kafka"      % TestcontainersVersion,
    "com.dimafeng"                 %% "testcontainers-scala-mongodb"    % TestcontainersVersion,
    "com.dimafeng"                 %% "testcontainers-scala-scalatest"  % TestcontainersVersion)
}
