package mobimeo.challenge

import cats.effect.unsafe.implicits.global
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers
import CsvDecoders.*
import java.time.LocalTime

class CsvDecodersSpec extends AnyFreeSpec with Matchers:

  "lines are ok" in {
    CsvDecoders.lines.compile.toList.unsafeRunSync() shouldBe List(
      Line("0", "M4"),
      Line("1", "200"),
      Line("2", "S75"))
  }

  "stops are ok" in {
    CsvDecoders.stops.compile.toList.unsafeRunSync().take(3) shouldBe List(
      Stop("0", 1, 1),
      Stop("1", 1, 4),
      Stop("2", 1, 7))
  }

  "times are ok" in {
    CsvDecoders.times.compile.toList.unsafeRunSync().take(3) shouldBe List(
      Time("0", "0", LocalTime.of(10, 0)),
      Time("0", "1", LocalTime.of(10, 2)),
      Time("0", "2", LocalTime.of(10, 5)))
  }

  "delays are ok" in {
    CsvDecoders.delays.compile.toList.unsafeRunSync() shouldBe List(
      Delay("M4", 1),
      Delay("200", 2),
      Delay("S75", 10))
  }
