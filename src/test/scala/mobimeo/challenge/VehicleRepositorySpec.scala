package mobimeo.challenge

import cats.effect.unsafe.implicits.global
import java.time.LocalTime
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers
import CsvDecoders.*

class VehicleRepositorySpec extends AnyFreeSpec with Matchers:

  "findLine" - {
    "finds M4 at 1,1 at 10:00 (+ delay)" in {
      val actual = VehicleRepository.findLine((1,1), LocalTime.of(10, 1)).compile.toList.unsafeRunSync()
      actual shouldBe List("M4")
    }

    "finds S75 at 4,9 at 10:13 (+ delay)" in {
      val actual = VehicleRepository.findLine((4,9), LocalTime.of(10, 23)).compile.toList.unsafeRunSync()
      actual shouldBe List("S75")
    }
  }

  "findNext" - {
    "finds M4 at 1,1 at 10:00 (+ delay)" in {
      val actual = VehicleRepository.findNext((1,1), LocalTime.of(10, 1)).compile.toList.unsafeRunSync()
      actual shouldBe List("M4")
    }

    "finds S75 at 4,9 at 10:13 (+ delay)" in {
      val actual = VehicleRepository.findNext((4,9), LocalTime.of(10, 13)).compile.toList.unsafeRunSync()
      actual shouldBe List("S75")
    }
  }
