package mobimeo.challenge

import cats.effect.*
import cats.effect.unsafe.implicits.global
import org.apache.logging.log4j.LogManager.getLogger
import org.http4s.*
import org.http4s.Method._
import org.http4s.blaze.client.BlazeClientBuilder
import org.http4s.client.dsl.io._
import org.http4s.client.middleware.RequestLogger
import org.http4s.headers.Accept
import org.http4s.implicits.*
import org.scalatest.BeforeAndAfterAll
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class VerspaetungApiSpec extends AnyFreeSpec with Matchers with BeforeAndAfterAll:
  private val logger = getLogger(getClass)

  "The API" - {
    "finds a vehicle for a given time and X & Y coordinates" in {
      run("vehicles", "3,4", "10:06") shouldBe "[\"200\"]"
    }

    "returns the vehicle arriving next at a given stop" in {
      run("nextvehicle", "3,4", "10:03") shouldBe "\"200\""
    }

    "indicates if a given line is currently delayed" in {
      run("delays", "M4") shouldBe "1"
    }
  }

  private val serverUnderTest =
      Main.run(Nil).start.unsafeRunSync()

  override def beforeAll(): Unit =
    Thread.sleep(1000)
    logger.info(s"Test server running with cancel token $serverUnderTest")

  override def afterAll(): Unit =
    logger.info("Stopping test server")
    serverUnderTest.cancel.unsafeRunSync()

  private def run(segments: String*) =
    val request =
      GET(
        segments.foldLeft(uri"http://localhost:8081") { _ / _ },
        Accept(MediaType.application.json))

    BlazeClientBuilder[IO]
      .resource
      .map { RequestLogger(logHeaders = true, logBody = true, logAction = Some(s => IO(logger.debug(s)))) }
      .use { _.expect[String](request) }
      .unsafeRunSync()
