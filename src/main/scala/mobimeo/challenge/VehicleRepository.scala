package mobimeo.challenge

import cats.effect.*
import fs2.Stream
import java.time.LocalTime
import java.time.temporal.ChronoUnit.MINUTES
import org.apache.logging.log4j.LogManager.getLogger
import scala.concurrent.duration.DurationInt
import CsvDecoders.*

object VehicleRepository:
  private val logger = getLogger(getClass)

  def findDelay: Stream[IO, (String, Int)] =
    CsvDecoders.delays.map { case Delay(line, time) => line -> time }

  def findLine(position: (Int, Int), time: LocalTime): Stream[IO, String] =
    logger.debug(s"findLine($position, $time)")
    val positions =
      for
        line   <- lines
        stop   <- stops
        delay  <- delays if line.name == delay.lineName
        time   <- times if line.id == time.lineId && stop.id == time.stopId
      yield Vehicle(line.name, (stop.x, stop.y), time.time.plus(delay.delay, MINUTES))

    positions.collect {
      case Vehicle(lineName, p, t) if p == position && t == time => lineName
    }

  def findNext(position: (Int, Int), time: LocalTime): Stream[IO, String] =
    val next = for
      t  <- Stream.iterate(time) { _.plus(1, MINUTES) }
      l  <- findLine(position, t)
    yield l
    next.head.timeout(3.seconds)

case class Vehicle(lineName: String, position: (Int, Int), time: LocalTime)
