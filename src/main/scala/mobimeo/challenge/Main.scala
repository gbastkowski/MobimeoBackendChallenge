package mobimeo.challenge

import cats.effect.*
import cats.implicits.*
import java.time.LocalTime
import org.apache.logging.log4j.LogManager.getLogger
import org.http4s.HttpRoutes
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.server.Router
import scala.concurrent.ExecutionContext
import scala.io.Source.fromResource
import sttp.tapir.server.http4s.Http4sServerInterpreter
import CsvDecoders.*

object Main extends IOApp:
  private val logger = getLogger(getClass)

  /**
    * Start the application
    */
  def run(args: List[String]): IO[ExitCode] =
    val interpreter = Http4sServerInterpreter[IO]()
    import interpreter.*

    val routes = Router(
      "/vehicles"    -> toRoutes(Endpoints.getVehicleByPositionAndTime   serverLogic getVehicleAt),
      "/nextvehicle" -> toRoutes(Endpoints.getVehicleByPositionInFuture  serverLogic getNextVehicle),
      "/delays"      -> toRoutes(Endpoints.getLineDelay                  serverLogic getLineDelay))

    BlazeServerBuilder[IO]
      .bindHttp(8081)
      .withHttpApp(routes.orNotFound)
      .serve
      .compile
      .lastOrError

  private def getVehicleAt(x: Int, y: Int, time: LocalTime): IO[Either[Unit, List[String]]] =
    VehicleRepository
      .findLine((x, y), time)
      .debug(s => s"getVehicleAt($x, $y, $time) found $s", logger.debug)
      .compile.toList
      .map { Right.apply }

  private def getNextVehicle(x: Int, y: Int, time: LocalTime): IO[Either[Unit, String]] =
    VehicleRepository
      .findNext((x, y), time)
      .debug(s => s"getNextVehicle($x, $y, $time) found $s", logger.debug)
      .head.compile.last
      .map { _.toRight(()) }

  private def getLineDelay(line: String): IO[Either[Unit, Int]] =
    VehicleRepository.findDelay
      .collect { case (l, d) if (line == l) => d }
      .head.compile.last
      .map { _.toRight(()) }
