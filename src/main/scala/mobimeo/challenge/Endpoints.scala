package mobimeo.challenge

import java.time.temporal.ChronoField
import java.time.LocalTime
import sttp.tapir._
import sttp.tapir.docs.openapi._
import sttp.tapir.json.circe._
import sttp.tapir.model._
import sttp.tapir.openapi._
import sttp.tapir.openapi.circe.yaml._
import sttp.tapir.CodecFormat.TextPlain
import scala.util.Try
import scala.util.Success

object Endpoints extends TapirJsonCirce:

  def getVehicleByPositionAndTime: Endpoint[Unit, (Int, Int, LocalTime), Unit, List[String], Any] =
    endpoint
      .get
      .name       { "Get vehicles" }
      .description{ """Find a vehicle for a given time and X & Y coordinates""" }
      .in         { path[(Int, Int)]        name    "position"  description """the requested position""" }
      .in         { path[LocalTime]         name    "time"      description """the requested time""" }
      .out        { jsonBody[List[String]]  example List("M4", "200")  }

  def getVehicleByPositionInFuture: Endpoint[Unit, (Int, Int, LocalTime), Unit, String, Any] =
    endpoint
      .get
      .name       { "Get upcoming vehicles" }
      .description{ """Return the vehicle arriving next at a given stop""" }
      .in         { path[(Int, Int)]        name    "position"     description """the requested position"""  }
      .in         { path[LocalTime]         name    "time"         description """the requested time""" }
      .out        { jsonBody[String]        example "M4"  }

  def getLineDelay: Endpoint[Unit, String, Unit, Int, Any] = endpoint
      .get
      .name       { "Get delays" }
      .description{ """Indicate if a given line is currently delayed""" }
      .in         { path[String]            name    "line"         description """the requested line""" }
      .out        { jsonBody[Int]           example 12  }

  private given Codec[String, (Int, Int), TextPlain] =
    def decode(s: String) =
      s.split(",").map(s => Try(Integer.parseInt(s))) match
        case Array(Success(x), Success(y)) => DecodeResult.Value((x, y))
        case _                             => DecodeResult.Error(s, new IllegalArgumentException("was not (x,y)"))

    def encode(tuple: (Int, Int)) = s"${tuple._1},${tuple._2}"

    Codec.string.mapDecode(decode)(encode)
