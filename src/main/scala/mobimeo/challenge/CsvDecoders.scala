package mobimeo.challenge

import cats.effect.*
import fs2.*
import fs2.data.csv.*
import fs2.data.csv.generic.semiauto.*
import java.time.LocalTime
import mobimeo.challenge.CsvDecoders.Line
import scala.io.Source.fromResource

object CsvDecoders:

  val delays: Stream[IO, Delay] = streamResource[Delay]("delays.csv")
  val lines: Stream[IO, Line] = streamResource[Line]("lines.csv")
  val stops: Stream[IO, Stop] = streamResource[Stop]("stops.csv")
  val times: Stream[IO, Time] = streamResource[Time]("times.csv")

  case class Delay(lineName: String, delay: Int)
  case class Line(id: String, name: String)
  case class Stop(id: String, x: Int, y: Int)
  case class Time(lineId: String, stopId: String, time: LocalTime)

  private given RowDecoder[Delay] = deriveRowDecoder
  private given RowDecoder[Line] = deriveRowDecoder
  private given RowDecoder[Stop] = deriveRowDecoder
  private given RowDecoder[Time] = deriveRowDecoder

  private def streamResource[T](name: String)(using RowDecoder[T]): Stream[IO, T] =
    Stream
      .emit(fromResource(name).mkString)
      .covary[IO]
      .through(decodeSkippingHeaders[T]())
